import { shallowMount } from "@vue/test-utils";
import Checkbox from "@/components/Checkbox.vue";
import { nextTick } from "./utils";

describe("Checkbox.vue", () => {
  const wrapper = shallowMount(Checkbox);

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it("renders correctly", () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it("support checked prop", async () => {
    const wrapper = shallowMount(Checkbox);
    wrapper.setProps({ checked: true });

    await nextTick();

    expect(wrapper.element).toMatchSnapshot();
  });
});

# ⚠️ Archived

### [Task](./Task.md)
### [Design](./design/index.png)

## Stands

| Stage | Branch |
| --- | --- |
| **[Production](https://product-nutrition.vercel.app/)** | `release` |
| [Testing](https://product-nutrition-test.vercel.app/) | `master` |


## Setup commands

```bash
# Install deps
npm install

# Compiles and hot-reloads for development
npm run serve

# Compiles and minifies for production
npm run build

# Tests
npm run test

# Lints and fixes files
npm run lint
```

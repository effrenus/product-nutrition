## TODO list

* Component for icons.
* ✅ Disable controls while processing delete, ~~no column selected~~.
* ✅ More ~~intelligent~~ simple prompt component.
* 🐛 Incorrect behaviour after deleting all items on last page.
* Precommit hooks.
* Trigger build/deploy for specific targets (files/dirs).
* Integration/unit tests.
* File an issue instead of Todo.md 🤔

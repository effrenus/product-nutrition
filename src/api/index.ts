const MAX_DELAY = 3000;
const runAfterRandDelay = (fn: Function) =>
  setTimeout(fn, Math.trunc(Math.random() * MAX_DELAY));

/** True = 65%, False = 35% */
const rejectByChance = () => Math.random() <= 0.35;

/** Emulate get request */
export const getProducts = () =>
  new Promise((resolve, reject) => {
    runAfterRandDelay(
      rejectByChance()
        ? () => reject({ error: "Server error" })
        : () =>
            fetch("/data/products.json")
              .then(res => res.json())
              .then(resolve)
    );
  });

/** Emulate delete request */
export const deleteProducts = () =>
  new Promise((resolve, reject) => {
    runAfterRandDelay(
      rejectByChance()
        ? () => reject({ error: "Server error" })
        : () => resolve({ message: "deleted" })
    );
  });

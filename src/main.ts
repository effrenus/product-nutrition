import Vue from "vue";
import ClickOutside from "vue-click-outside";
import ProductsPage from "./ProductsPage.vue";
import store from "./store";

Vue.config.productionTip = false;
Vue.directive("click-outside", ClickOutside);

new Vue({
  store,
  render: h => h(ProductsPage)
}).$mount("#app");

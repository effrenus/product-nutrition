import * as api from "../../api";

interface Product {
  id: string;
  product: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
  iron: number;
}

export enum LoadingState {
  NOT_LOADED = 1,
  FETCHING,
  LOADED,
  ERROR
}

export type ProductField = [string, string, number]; // label, title, column width

export interface State {
  products: Product[];
  fields: ProductField[];
  retrieveState: LoadingState;
}

interface Store {
  commit: Function;
}

export default {
  state: {
    products: [],
    fields: [
      ["product", "Product (100g serving)", 250],
      ["calories", "Calories", 100],
      ["fat", "Fat (g)", 100],
      ["carbs", "Carbs (g)", 100],
      ["protein", "Protein (g)", 100],
      ["iron", "Iron (%)", 100]
    ],
    retrieveState: LoadingState.NOT_LOADED
  },
  mutations: {
    add(state: State, ps: Product[]) {
      state.products.push(...ps);
      state.retrieveState = LoadingState.LOADED;
    },
    delete({ products }: State, ids: string[]) {
      for (let i = products.length - 1; i >= 0; --i) {
        if (ids.includes(products[i].id)) {
          products.splice(i, 1);
        }
      }
    },
    status(state: State, value: LoadingState) {
      state.retrieveState = value;
    }
  },
  actions: {
    getProducts({ commit }: Store) {
      const loadPromise = api.getProducts();

      commit("status", LoadingState.FETCHING);

      loadPromise
        .then(products => {
          commit("add", products as Product[]);
          commit("status", LoadingState.LOADED);
        })
        .catch(() => commit("status", LoadingState.ERROR));
    },
    deleteProducts({ commit }: Store, ids: string[]) {
      const promise = api.deleteProducts();

      promise
        .then(() => commit("delete", ids))
        .catch(err => console.error(err));

      return promise;
    }
  }
};

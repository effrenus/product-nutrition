import Vue from "vue";
import Vuex from "vuex";
import productModule from "./modules/products";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    product: {
      namespaced: true,
      ...productModule
    }
  }
});
